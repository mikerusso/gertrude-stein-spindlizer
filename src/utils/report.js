//
const NGrams=require('natural').NGrams;
const ngram = NGrams;
const WordTokenizer = require('natural').WordTokenizer;
const tokenizer = new WordTokenizer();

function fullReport(text){
    const tokens = tokenizer.tokenize(text);
    const one = counter(tokens);
    const two = gramReport(tokens, 2);
    const three = gramReport(tokens, 3);
    const four = gramReport(tokens, 4);
    const five = gramReport(tokens, 5);
    const six = gramReport(tokens, 6);
    const report = one.concat(two, three, four, five, six);
    // report.sort(function(a, b) { return b.count - a.count; });
    report.sort(function(a, b) { return b.score - a.score; });
    return report;
}

function winners(text){
    let working = text;
    let winners =[];
    const first = fullReport(working)[1].key.split('-').join(' ');
    winners.push(first);
    working = working.split(first).join(' ');
    const second = fullReport(working)[1].key.split('-').join(' ');
    winners.push(second);
    working = working.split(second).join(' ');
    const third = fullReport(working)[1].key.split('-').join(' ');
    winners.push(third);
    working = working.split(third).join(' ');
    const fourth = fullReport(working)[1].key.split('-').join(' ');
    winners.push(fourth);
    working = working.split(fourth).join(' ');
    const fifth = fullReport(working)[1].key.split('-').join(' ');
    winners.push(fifth);
    working = working.split(fifth).join(' ');
    // winners.push(second);
    // working.replaceAll(second, ' ');
    // const third = fullReport(working)[1].key.replaceAll('-', ' ');
    // winners.push(third);
    // working.replaceAll(third, ' ');
    return winners
}

function gramReport (tokens, gram){
    const grams = ngram.ngrams(tokens, gram);
    const report = counter(grams);
    report.forEach((entry,index)=>{
        report[index].words = gram;
    });
    return report;
}
/**
 * This function will take arbritrary array of tokens and return a report of the token frequency
 * @param {Array} tokens - Parameter description.
 * @return {Array} - an array, sorted, of token frequency
 */
function counter (tokens) {
    let report = {}
    tokens.forEach((token, index)=>{
        const id = Array.isArray(token) ? token.join('-') : token;
        if (report[id]){
            report[id].count += 1;
            report[id].indexes.push(index);
        } else {
            report[id]={count: 1, indexes: [index]};
        }
    });
    return sortObject(report);
}

/**
 * Takes an object of counts (produced by 'counter' initially), and gives a sorted array of objects and keys
 * @param {Object} obj - Object produced by counter, where an object has many "properties" for each distinct token.
 * @returns {Array} Returns a sorted array.
 */
function sortObject(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'count': obj[prop].count,
                'indexes': obj[prop].indexes,
                'words': prop.split('-').length,
                'score': prop.split('-').length * obj[prop].count
            });
        }
    }
    return arr; // returns array
}
module.exports = winners;

//
