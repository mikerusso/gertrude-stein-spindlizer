
const natural =require('natural');
const distance = natural.JaroWinklerDistance;
const beginningText="Some time then there will be every kind of history of every one who ever can or is or was or will be living.  Some time then there will be a history of every one from their beginning to their ending.  Sometime then there will be a history of all of them, of every kind of them, of every one, of every bit of living they ever have in them, of them when there is never more than a beginning to them, of every kind of them, of every one when there is very little beginning and then there is an ending, there will then sometime be a history of every one there will be a history of everything that ever was or is or will be them, of everything that was or is or will be all of any one or all of all of them.  Sometime then there will be a history of every one, of everything or anything that is all them or any part of them and sometime then there will be a history of how anything or everything comes out from every one, comes out from every one or any one from the beginning to the ending being in them.  Sometime then there must be a history of every one who ever was or is or will be living.  ";

//Something to consolidate the sub words...
//we have a set, we want to iterate each word and if it is contained in another word, then that word its contained in is a compound
// we delete the compound, then make the base word a regex of itself. 
function consolidate(set){
    let oldSet = set;
    let newEntries =[];
    //generating subwords
    for(let i=0;i<set.length;i++){
        let currWord = set[i];
        let entry;
        if(currWord.length>2){
            for(let j=0; j<set.length;j++){
                if(oldSet[j]){
                    if(oldSet[j].includes(currWord)&&i!=j){
                        entry="\\s*\\w*"+oldSet[i]+"\\w*\\s*";
                        break;
                    }
                }
            }
            // entry ? newEntries.push(entry):null;

        }
    }
    //filling up the rest of the set
    let regSet= newEntries.map((word)=>new RegExp(word,"gi"));
    let leftovers=set.filter((word)=>{
        let found = false;
        regSet.forEach((reg)=>{if (word.match(reg)||word===""){found=true;}});
        if(found){return false;} else {return true;}
    });
    let finalReturn=regSet.concat(leftovers.map(entry=> new RegExp(entry,"gi"))); 
    finalReturn=finalReturn.filter((entry)=>entry!=/(?:)/gi);
    return finalReturn;
}
function checkReg(word,regArray){
    let finds=-1;
    regArray.forEach((reg,index)=>{
        if(word.search(reg)!=-1){
            finds = index;
        } 
    });
    // console.log("this word... "+word+"  was found at the this index  "+finds);
    return finds;
}
function spindelizer(text){
    const tokens= text.replace(/[,.]/gi,"").split(" ");
    const wordGroups = consolidate([...new Set(tokens)]);
    let currLine=0;
    let currColumn=0;
    let lines=[[]];
    /*
      Ok, so iterate through the list, placing words.
      If the word is repeating, make a new line and place the word in the same place as the word above it
      So we need a running reference of array places and their words; a sense of new lines.
      Really, we just need to know what words and their order for each line,
      and we can align them later.
    */
    tokens.forEach((token)=>{
        //First check if this word has been used on the current line already
        let foundCol = checkReg(token,unique);
        if(lines[currLine].includes(token)){
            currLine++;
        } else {
            
        }
        // console.log(unique.indexOf(token));
        lines[currLine][foundCol]=token;
        currColumn=foundCol;
    });
    return lines;
}
// console.log(natural.JaroWinklerDistance("dixon","dicksonx"));
// export default spindelizer;
function distanceConsolidate(set){
    let oldSet = set;
    let consolidated =[];
    //generating subwords
    for(let i=0;i<set.length;i++){
        let currWord = set[i];
        let entries =[currWord]; //
        for(let j=0; j<set.length;j++){
            if(distance(currWord,set[j])>=0.9 && i!=j){
                entries.push(oldSet[j]);
                oldSet.splice(j,1);
            }
        }
        oldSet.splice(i,1);
        consolidated.push(entries);
    }
    return new Set(consolidated);
}

function sortFromText(groups,text){
    let oldGroups=groups;
    let count =oldGroups.length;
    let newGroups=[];
    const words=text.replace(/[,.]/gi,"").split(" ");
    for(let i=0;i<words.length;i++){
        for(let j=0;j<oldGroups.length;j++){
            if(oldGroups[j].includes(words[i])){
                newGroups.push(oldGroups[j]);
                oldGroups.splice(j,1);
                break;
            }
        }
        if(oldGroups.length<0){break;}
    }
    console.log(oldGroups.length);
    return newGroups;
}
const tokens= beginningText.replace(/[,.]/gi,"").split(" ");
let unique = [...new Set(tokens)];
let consolidated = distanceConsolidate(unique);
// let sorted = sortFromText(consolidated,beginningText);
