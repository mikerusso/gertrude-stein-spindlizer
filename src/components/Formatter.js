import React from "react";

const highlighted={

    // backgroundColor:"#FFFF00"
    fontWeight:"bold"

};

export default function Formatter(props){
    let text =props.text;
    let phraseArray="";
    const rules = props.rules;
    // const rules = [{
    //     phrase: "a history",
    //     style:{fontWeight:"bold"}
    // }]
    rules.forEach((rule,index)=>{
        //create some dummy html to just parse later
        // console.log(rule.phrase);
        text=text
            .split(rule.phrase)
            .join(`<phrase>${rule.phrase};;${index}<phrase>`);

        phraseArray=text.split("<phrase>")
            .map((phrase,index)=>{
                if(phrase.includes(";;")){
                    const arr = phrase.split(";;");
                    return [arr[0],arr[1]];
                } else{
                    return phrase;
                }
            });
    });

    return(
        <p>
          {phraseArray
           ?
           phraseArray.map((entry,index)=>{
              return typeof entry === 'string'
                   ?
                   entry
                   :
                   <span className="highlighted" key={index} style={rules[entry[1]].style}> { entry[0]  }</span>;
          })
           :
           null}
        </p>
    )
}
