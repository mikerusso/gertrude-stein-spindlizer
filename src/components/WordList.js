import React from 'react';
import WordEntry from './WordEntry';

const WordTokenizer = require('natural').WordTokenizer;
// const tokenizer = new natural.WordPunctTokenizer();
const tokenizer = new WordTokenizer();

function uniqueWords(text){
    let tokens = tokenizer.tokenize(text);
    return [...new Set(tokens)];
}

function WordList(props){
    const words = uniqueWords(props.currText);
    const listStyle={
        maxHeight:"50vh",
        overflow:"auto"
    }
    return(
        <ul style={listStyle}>
          { words.map((word,index)=><li key={index}><WordEntry word={word}/></li>) }
        </ul>
    )
}
export default WordList;
