/**
 * @fileOverview Trying to recreate Gass' "spindle diagram" from him preface to Stein
 * @name Spindle.js
 * @author 
 * @license 
 */


import React from 'react';
import spindelizer from '../utils/spindelizer'

export default class Spindle extends React.Component {
    constructor(props){
        super(props);
        const split = props.currText.split(" ");
        const unique=[...new Set(split)] 
        // console.log(props);
        this.state=
            { 
                used:[],
                text: this.props.currText,
                header:unique,
                lines:[]
            };
    }

    render(){
        const lines = (spindelizer(this.props.currText));
        console.log(lines);
        return(
            <table style={{border:"1px solid black"}}>
              <thead>
                
              </thead>
              <tbody style={{border:"inherit"}}>

                {lines.map((line)=>{
                    return(
                        <tr style={{border:"inherit"}}>
                          {line.map((word)=>{
                              return(
                                  <td style={{border:"inherit"}}>
                                    {word}
                                  </td>
                              )
                          })}
                        </tr>
                    )
                })}
              </tbody>
            </table>
            
        );
    }

}
