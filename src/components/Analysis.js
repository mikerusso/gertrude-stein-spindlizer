import React from 'react';
import Displayer from './Displayer';
const report = require('../utils/report');
const randomColor = require('random-color');

const containerStyle={
    // overflow: "auto",
    // height: "100px",
    // minHeight:"50%",
    display: "flex",
    justifyContent: "space-around",
    flexDirection: "column",
    // fontFamily: "'Roboto', sans-serif",
    textAlign: "center",
    fontWeight: "darker",
    border: "4px solid black"
}
const inputterStyle={
    minWidth:"50%",
    height:"20%",
    float: "left",
    border: "inherit"
};
let colors = []
for(let i =0;i<8;i++){
    let thiscolor=randomColor();
    colors.push(thiscolor.hexString());
}
const beginningText="Some time then there will be every kind of history of every one who ever can or is or was or will be living.  Some time then there will be a history of every one from their beginning to their ending.  Sometime then there will be a history of all of them, of every kind of them, of every one, of every bit of living they ever have in them, of them when there is never more than a beginning to them, of every kind of them, of every one when there is very little beginning and then there is an ending, there will then sometime be a history of every one there will be a history of everything that ever was or is or will be them, of everything that was or is or will be all of any one or all of all of them.  Sometime then there will be a history of every one, of everything or anything that is all them or any part of them and sometime then there will be a history of how anything or everything comes out from every one, comes out from every one or any one from the beginning to the ending being in them.  Sometime then there must be a history of every one who ever was or is or will be living.  ";
function outliner(string){
    let outline =[];
    string.split(/\n\n/g).forEach((sentence)=>{
        let phrases = [];
        sentence.split(/\n/g).forEach((phrase)=>{
            phrases.push(phrase);
        })
        outline.push(phrases);
    })
    return outline;
}
class Analysis extends React.Component {
    constructor(props){
        super(props);
        this.state={currText:"", winners:[], rules:[],outline:[]}
    }

    handleInput=(e)=>{
        // console.log(e.target.value) ;
        const winners = report(e.target.value);
        let newRules = [];
        winners.forEach((winner)=>{
            newRules.push({phrase:winner,style:{fontWeight:"bold"}});
        });
        console.log(outliner(e.target.value));
        let outline = outliner(e.target.value);
        this.setState({
            currText:e.target.value,
            rules: newRules,
            outline:outline
        });
    }

    componentDidMount(){
        const winners = report(beginningText);
        let newRules = [];
        let outline = outliner(beginningText);
        winners.map((winner)=>{
            newRules.push({phrase:winner,style:{fontWeight:"bold"}});
        });
        this.setState({currText:beginningText, winners:winners, rules: newRules, outline:outline});
    }

    render(){
        return(
        <div id="container" style={containerStyle}>
          <textarea defaultValue={this.state.currText} style={inputterStyle} onChange={this.handleInput}>
          </textarea>
          {this.props.displayOn ? 

           <Displayer currText={this.state.currText} rules={this.state.rules} outline={this.state.outline}/>
           : null}
        </div>
        );
    }
}
export default Analysis;
