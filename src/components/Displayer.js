import React from 'react';
import WordList from './WordList';
// import Highlighter from './Highlighter';
import Outliner from './Outliner';
import Formatter from './Formatter';
import Spindle from './Spindle';
const pStyle = {margin:"0", padding:"0"}

function Displayer(props){
    const displayerStyle={
        border: "inherit",
        width:"100%",
        height:"100%",
    };

    return( 
        <div style={displayerStyle}>
          {/* <Formatter text={props.currText} rules={props.rules}/> */}
          {/* <WordList currText={props.currText} />  */}
          {/* {console.log(props.currText)} */}
          <Spindle currText={props.currText}/>
        </div>
    );
}

export default Displayer;
