import React from 'react';
import Analysis from './components/Analysis';

const appStyle={
    width:"100%",
    height:"100%",
    display:"block"

};

const analysisStyle={
    height:"100%"
};
class App extends React.Component {
    constructor(props){
        super(props);
        this.state={display:true};
    }
    handleHide = (e) => {
        if(this.state.display){
            this.setState({display: false});
        } else {
            this.setState({display: true});
        }
    }

    render(){
        return(
        <div className="App" style={appStyle}>
          <div className="header"><span><h1>text analysis</h1><button onClick={this.handleHide}>Hide windows</button></span></div> 
          <Analysis style={analysisStyle} displayOn={this.state.display}/>
        </div>
        );
    };
}

export default App;
